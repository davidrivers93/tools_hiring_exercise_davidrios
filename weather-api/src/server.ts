import { ForecastService } from './forecast.service';
import * as express from "express";
import * as path from "path";
import { GoogleController } from "./google.service";

import * as dotenv from "dotenv";

// initialize configuration
dotenv.config();

const app = express();
app.set("port", 3000);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var http = require("http").Server(app);

// set up socket.io and bind it to our
// http server.
let io = require("socket.io")(http);

const googleController = new GoogleController();
const forecastService = new ForecastService();
const intervalTime = 5; //Interval in minutes
let interval:any;

const getGeocodeData = (city: string, socket: any) => googleController.getGeocodeData(city).then((result: any) => {
  console.log("Get data");
  forecastService.getForecast(result.lat, result.lng)
    .then((res) => {
      console.log(`Forecast for ${city} searched`, res);
      socket.emit("forecast", res);
    });
});

const intervalFunc = (city: string, socket: any) => {
  interval = setInterval(() => {
    getGeocodeData(city, socket);
  }, intervalTime*60*1000)
};



app.get("/", (req: any, res: any) => {
  res.sendFile(path.resolve("./src/index.html"));
});



/** 
* get /cities/* - Rest service that creates socket for weather
*
* 
* @param city(string) City that it's necessary to retrieve his location
*
* @returns Returns an object based on the city location
*/
app.get("/cities/:city", (req: any, res: any) => {
  
  const city:string = req.params.city;
  console.log(`City ${city} searched`);
  io.of(`/cities/${city}`).once("connection", function (socket: any) {
    console.log(`${socket.id} connected`);
    socket.on("disconnect",function () {
      clearInterval(interval);
      socket.disconnect();
      console.log(`${socket.id} disconnected`);
    });
    //return first forecast
    getGeocodeData(city,socket);

    //Creates 5 minutes interval
    intervalFunc(city, socket);

  });
  res.send(JSON.stringify({connect: true}));
});

// start our simple server up on localhost:3000
const server = http.listen(3000, function () {
  console.log("listening on *:3000");
});

module.exports = app