import { GoogleController } from './../google.service';


import * as dotenv from "dotenv";

describe('basic socket.io example', function() {
  it('should return redis data', (done) => {
    dotenv.config();

    // once connected, emit Hello World
    let googleController = new GoogleController();
    
    googleController.getRedisData("valencia")
    .then(res =>  {
      const result = JSON.parse(res);
      expect(typeof result === "object").toBe(true);
      expect(typeof result.lat === "number").toBe(true);
      expect(typeof result.lng === "number").toBe(true);
      done();
    });

  });

  it('should not return redis data', (done) => {
    dotenv.config();

    // once connected, emit Hello World
    let googleController = new GoogleController();
    
    googleController.getRedisData("Reikiavik")
    .then((res:any) =>  {
      expect(res).toBeNull();
      done();
    });

  });

});
