import { ForecastService } from './../forecast.service';
var io = require('socket.io-client');

import * as dotenv from "dotenv";

describe('basic socket.io example', function() {

  var socket:any;
  var io_server:any;

  beforeEach(function(done) {
    io_server = require('socket.io').listen(3001);
    // initialize configuration
    dotenv.config();

    // Setup
    socket = io.connect('http://localhost:3001', {
      'reconnection delay' : 0
      , 'reopen delay' : 0
      , 'force new connection' : true
      , transports: ['websocket']
    });

    socket.on('connect', () => {
      done();
    });

    socket.on('disconnect', () => {
      console.log('disconnected...');
    });
  });

  afterEach((done) => {
    // Cleanup
    if(socket.connected) {
      socket.disconnect();
    }
    io_server.close();
    done();
  });

  it('should send a forecast', (done) => {
    // once connected, emit Hello World
    let forecastService = new ForecastService();
    forecastService.getForecast(39.46975,-0.37739).then((res) => io_server.emit('forecast', res));
    
    socket.once('forecast', (message:any) => {
      // Check that the message matches
      expect(typeof message === "object").toBe(true);
      done();
    });
  });

  it('should be disconnected', (done) => {
    socket.disconnect();
    expect(socket.disconnected).toEqual(true);
    expect(socket.connected).toEqual(false);
    done();
  });

  it('should be connected', (done) => {
    expect(socket.disconnected).toEqual(false);
    expect(socket.connected).toEqual(true);
    done();
  });

});
