var redis = require("redis");

export class RedisController{

    client:any;

    constructor(){
        console.log(`Connecting to ${process.env.REDIS}:6379`)
        this.client = redis.createClient({
            host: process.env.REDIS
        });
        this.client.on("error", function (err:any) {
            console.log("Error " + err);
        });
        
    }

    /**
     * getData - Retrieves data
     * 
     * @param search - City that is going to be fetched
     * 
     * @returns returns city data from redis 
     */
    async getData(search:string){
        return new Promise((resolve,reject) => {
            this.client.get(search, (err:string, reply:any) => {
                if(err){
                    reject(err);
                }
                resolve(reply);
            });
        });
    }

    /**
     * setData - Saves a location on redis
     * 
     * @param city key for redis
     * @param bounds lat and lang fetched on google
     */
    setData(city:string, bounds:any){
        this.client.set(city,JSON.stringify(bounds));
    }

}