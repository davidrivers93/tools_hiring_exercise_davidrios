const https = require('https');


export class ForecastService {
    constructor(){

    }

    /**
     * getForecast - Retrieves current forecast from forecast.io
     * 
     * @param forecast_location_lat - Location latitude
     * @param forecast_location_long  - Location longitude
     * @param forecast_units  - Location units. Standard measures as default
     * @param forecast_api_key - Api key connection
     */
    async getForecast(forecast_location_lat:number, 
                forecast_location_long:number,
                forecast_units="si"):Promise<any>{
        return new Promise((resolve,reject) => {
            https.get(`https://api.forecast.io/forecast/${process.env.FORECAST_API_KEY}/${forecast_location_lat},${forecast_location_long}?units=${forecast_units}`, (resp:any) => {
                let data = '';
                //A chunk of data has been recieved.
                resp.on('data', (chunk:any) => {
                    data += chunk;
                });
        
                //The whole response has been received. Print out the result.
                resp.on('end', () => {
                    resolve(JSON.parse(data).currently);
                });
            }).on("error", (err:any) => {
                reject(err);
            });
        });
    }
}