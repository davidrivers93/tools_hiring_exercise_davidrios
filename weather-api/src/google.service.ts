import { RedisController } from './redis.service';
const https = require('https');

export class GoogleController{

    redisController:RedisController = new RedisController();

    constructor(){

    }

    /** 
    * getGeocodeData - Function that retrieves the location data based on a location.
    *
    * @async
    * @param city(string) City that it's necessary to retrieve his location
    *
    * @returns Returns an object based on the city location
    */
    async getGeocodeData(city:string){
        const cityNormalized = this.normaliceCity(city);
        const redisData = await this.getRedisData(cityNormalized);
        if(!redisData){
            console.log(city + " not redis cached");
            const googleDataLocation = await this.getGoogleLocation(city);
            this.redisController.setData(cityNormalized,googleDataLocation);
            return googleDataLocation;
        }else{
            console.log(city + " redis cached");
            return JSON.parse(redisData as any);
        }    
    }

    /**
     * getGoogleLocation - Retrieves the location cordinates
     * 
     * @param city - City that needs to be searched
     * 
     * @returns location coordinates
     */
    private getGoogleLocation(city:string){
       return new Promise((resolve,reject) => {
           console.log(process.env);
            https.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${process.env.GOOGLEKEY}&address=${city}`, (resp:any) => {
                let data = '';
                //A chunk of data has been recieved.
                resp.on('data', (chunk:any) => {
                    data += chunk;
                });
        
                //The whole response has been received. Print out the result.
                resp.on('end', () => {
                    resolve(this.getCoordinates(data));
                });
            }).on("error", (err:any) => {
                reject(err);
            });
        });
    }

    async getRedisData(city:string):Promise<any>{
        return this.redisController.getData(city);
    }

    private normaliceCity(city:string):string{
        city = city.toLocaleLowerCase();
        return this.accentFold(city);
    }
    
    private accentFold(inStr: string) {
        return inStr.replace(
            /([àáâãäå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g,
            (str, a, c, e, i, n, o, s, u, y, ae): string => {
            if (a) return 'a';
            if (c) return 'c';
            if (e) return 'e';
            if (i) return 'i';
            if (n) return 'n';
            if (o) return 'o';
            if (s) return 's';
            if (u) return 'u';
            if (y) return 'y';
            if (ae) return 'ae';
            else return '';
            }
        );
    }

    private getCoordinates(response:any):any {
        console.log(response);
        return JSON.parse(response).results[0].geometry.location;
    }
}