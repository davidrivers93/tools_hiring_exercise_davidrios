# World temperatures

Betiware is a software company that has been tasked to develop a small application which will display the current temperature and the weather conditions for a particular city. The application will receive the city as URL parameter and display back the information.

To do so, Betiware has signed a collaboration agreement with Forecast.io to consume their API which provides weather data. Their API needs the location coordinates to be provided, unfortunately, the customer thinks it will be too complicated for the users to provided such information. The application will have to translate the name of the city to the latitude and longitude coordinates.

An example of the weather's API usage can be found in the following [example](https://gist.github.com/mjamieson/5274790#file-forecast-rb). The token to be used is `99f08919415972d78b757e13a6fc1345` in this case.

In order to retrieve the coordinates, the following Google Maps service can be used `http://maps.googleapis.com/maps/api/geocode/json?address=46021+Valencia,Spain&sensor=false`. The API needs a token as well, you can use the following:

There are other providers for the given services, so ideally, the application should be designed in a way that it can be switched easily to a different one further down the line.

Google's API limits the amount of free requests that can be performed every day. In anticipation of a high demand, Betiware would like to persist the City/Coordinates information to avoid making requests to an external provider when possible reducing their costs. Every time a new request comes, if the location information has already been requested in the past, it will fetch it from the local datastore. If the city information has never been requested before, the application will store it for future requests.

The same sort of agreement applies to the weather's provider API. Ideally, they would like to implement a kind of cache that avoids to make calls to the 3rd party API when the information has been requested in the last X minutes.

As an organization, Betiware is trying to transform its development cycle and to adopt some DevOps practices. Betiware has adopted agile methodologies and so far, all the deployments are manually done at the end of every Sprint (3 weeks).
The Organization is not happy with the approach and they would like to deploy as often as possible, without human intervention and without disrupting the customers with downtimes.

## Objectives
 * Build a front-end application which consumes Betiware's internal API as per the above description.
 * Build a backend application in any language which provides the simplified information to the frontend as per the above description
 * The applications should be 12 factor compliant
 * Add docker to both projects in a way that it is easy to work locally and deploy remotely.
 * Create a Pipeline configuration for a Continous Integration & Deployment tool of your choice (Jenkins, Travis, etc.) able to deploy to multiple environments

## The Front-end
A simple React application has been provided for the candidate to act as a client for the API requested. Instructions on its usage can be found in the project's [README](weather-dashboard/README.md).

The candidate will need to modify this project in a way that it can be deployed and connected to the API not just locally.


## The API
It is up to the candidate to complete the contents of the weather-api folder with any code thinks is necessary to complete the test. It can be developed in any language of his/her choice.

## The endpoint
The Dashboard application expects the API to implement, at least, the following endpoint.

```
GET http://weather-api:8080/cities/:name
```

The outcome of the following command will be similar to:
```json
{
  "time": 1556873490,
  "summary": "Partly Cloudy",
  "icon": "partly-cloudy-day",
  "precipIntensity": 0,
  "precipProbability": 0,
  "temperature": 18.49,
  "apparentTemperature": 18.49,
  "dewPoint": 12.93,
  "humidity": 0.7,
  "pressure": 1011.63,
  "windSpeed": 0.6,
  "windGust": 2.34,
  "windBearing": 267,
  "cloudCover": 0.48,
  "uvIndex": 3,
  "visibility": 10.01,
  "ozone": 345.8
}

```

The above JSON content is a simplification of the response from the Forecast.io API.

## The Cloud Infrastructure
It is up to the candidate to choose the Cloud provider he is more comfortable with. We can provide credentials for the Google Cloud and AWS services, but, if you want to use a different provider which offers a free tier you can use your own account.

## Hint
We appreciate that completing the 100% of the exercise in a timely manner can be challenging. If the candidate's does so, great. But we are more interested in observing practices, solving skills and how the problems are tackled. If not everything is completed, we would like to hear how the candidate would have solve it, why certain trade offs, etc. We don't want to overwhelm the candidate.
