This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>

## Folder Structure
The project should look like this:

```
weather-dashboard/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    components/*
    services/*
    App.css
    App.js
    App.test.js
    index.css
    index.js    
```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.

## The Application
Once the application is started, the city for which the user wants to know the weather, will be passed within the URL as a path parameter:
http://localhost:3000/valencia

Below is an example of the resultant screen:

![Screenshot](images/valencia.png)

##API Configuration
The API URL can be found in the [api](src/services/api.js) file. 

```javascript
var apiUrl = 'http://localhost:8080/cities/' + data.city.toLowerCase()
```

#One of the exercise challenges is to figure out how to build the project in a way in which the API url changes across environments.
