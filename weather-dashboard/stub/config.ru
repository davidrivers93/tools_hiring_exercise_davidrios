require 'sinatra'
require 'sinatra/cross_origin'

require 'json'

class Api < Sinatra::Base
  register Sinatra::CrossOrigin

  get '/cities/valencia' do
    cross_origin allow_origin: 'http://localhost:3000'

    status 200
    body File.read('./valencia.json')
  end

  get '/cities/boston' do
    cross_origin allow_origin: 'http://localhost:3000'

    status 200
    body File.read('./boston.json')
  end

end

run Api.new
