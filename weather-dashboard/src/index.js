import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

ReactDOM.render(<App />,document.getElementById('root'));
