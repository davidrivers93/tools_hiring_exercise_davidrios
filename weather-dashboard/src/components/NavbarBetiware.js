
import React from 'react';
import {withRouter} from 'react-router-dom';
import { Link } from 'react-router-dom'

class NavbarBetiware extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    this.props.history.push('/' + this.state.value);

    let cities = JSON.parse(localStorage.getItem("cities"))?JSON.parse(localStorage.getItem("cities")):[];

    if(!cities.find((city) => city.toLowerCase() === this.state.value.toLowerCase())){
      cities.push(this.state.value);
      localStorage.setItem("cities",JSON.stringify(cities));
    }

    event.preventDefault();
  }


  render() {
    return (
      <div>
        <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
          <div className="container">
            <div className="navbar-brand col-lg-3 mr-0 ">
              <Link className="text-center" to="/">Betiware <small>weather</small></Link>
            </div>
            <div className="col-lg-9 d-flex">
              <input className="form-control form-control-dark w-100" type="text" placeholder="Search city" aria-label="Search" value={this.state.value} onChange={this.handleChange}/>
              <ul className="navbar-nav px-3">
                <li className="nav-item text-nowrap">
                  <button className="nav-link" onClick={this.handleSubmit}>Search</button>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(NavbarBetiware);