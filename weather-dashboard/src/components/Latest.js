
import React from 'react';
import { Link } from 'react-router-dom'

class Latest extends React.Component{
    constructor(props) {
        super(props);
        this.state = {cities: ''};

        this.state = {cities:localStorage.getItem("cities")};
      }

      createCities = () => {
        let cities = []
        if(JSON.parse(this.state.cities)){
          JSON.parse(this.state.cities).reverse().forEach(city => {
            cities.push(
              <Link key={city} className="trigger  p-2 my-1 col-lg-12" to={this.cityLink(city)}>{city}</Link>)
          });
        }

        return cities
      }
    
      cityLink = (city) => {
        return "/" + city;
      }
    
      render() {
        return (
          <div className="col-lg-12">
            <p className="border-bottom pb-2 lead"><b>Latest searches</b></p>
            <div>
              {this.createCities()}
            </div>
          </div>
        );
      }
}


export default Latest;