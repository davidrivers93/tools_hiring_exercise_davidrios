
import React from 'react';
import {withRouter} from 'react-router-dom';


class Search extends React.Component{
  
    constructor(props) {
        super(props);
        this.state = {value: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleChange(event) {
        this.setState({value: event.target.value});
      }
    
      handleSubmit(event) {
        this.props.history.push('/' + this.state.value);

        let cities = JSON.parse(localStorage.getItem("cities"))?JSON.parse(localStorage.getItem("cities")):[];

        if(!cities.find((city) => city.toLowerCase() === this.state.value.toLowerCase())){
          cities.push(this.state.value);
          localStorage.setItem("cities",JSON.stringify(cities));
        }

        event.preventDefault();
      }


    
      render() {
        return (
          <form  onSubmit={this.handleSubmit}>
            <label>
              <input type="text" className="form-control" placeholder="City" value={this.state.value} onChange={this.handleChange} />
            </label>
            <input className="btn btn-primary bg-dark" type="submit" value="Submit" />
          </form>

          
        );
      }
}


export default withRouter(Search);