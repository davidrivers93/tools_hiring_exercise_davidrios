import React, { Component } from 'react'
import services from '../services/api.js'
import Skycons from 'react-skycons'

import socketIOClient from "socket.io-client";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export class CityWeather extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {},
      city: props.cityId,
      found: true,
      loading: true
    }
  }

  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }


  renderLoadingDiv() {
    return (<div className="loadingDiv">Loading...</div>)
  }

  renderInfo() {
    if (this.state.found) {
      var icon = this.state.data.icon.replace(/-/g, '_').toUpperCase()
      return (
        <div className="weather-info">
          <h1 className="text-left border-bottom mb-4">Weather of {this.state.city}</h1>
          <div className="row">
          <div className="col-lg-4">
            <div className="current">
              <div className="card card-shadow">
              <div className="weather-icon bg-primary  p-3">
                <FontAwesomeIcon icon="thermometer-three-quarters" size="6x" inverse/>
                </div>
                <div className="info  py-4">
                  <div><p className="lead border-bottom">Temperature</p></div>
                  <div className="temp">Temperature: {this.state.data.temperature}<small>º</small></div>
                  <div className="wind">Apparent temperature: {this.state.data.apparentTemperature}<small>º</small></div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="current">
              <div className="card card-shadow">
              <div className="weather-icon bg-primary  p-3">
                  <Skycons color='white' icon={icon} autoplay={true} style={{ width: 100 + '%' }} />
                </div>
                <div className="info  p-3">
                  <div><p className="lead border-bottom">Weather</p></div>
                  <div className="wind">Summary :  {this.state.data.summary} </div>
                  <div className="city">Humidity:  {this.state.data.humidity * 100}<small>%</small></div>
                  <div className="temp">Pressure : {this.state.data.pressure}<small>psi</small></div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="current">
              <div className="card card-shadow">
              <div className="weather-icon bg-primary  p-3">
                <FontAwesomeIcon icon="wind" size="6x" inverse/>
                </div>
                <div className="info  p-3">
                  <div><p className="lead border-bottom">Wind</p></div>
                  <div className="city">Direction :  {this.windDirection()}</div>
                  <div className="temp">Speed : {this.state.data.windSpeed}<small>km/h</small></div>
                  <div className="wind">Gust :  {this.state.data.windGust} <small>km/h</small></div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          </div>

        </div>)
    } else {
      return (<h2>City not found</h2>)
    }
  }

  componentDidMount(props) {
    this.setState({ loading: true, city: this.state.city }, this.fetchData(this.state.city));
  }

  windDirection(){
    const angle = this.state.data.windBearing;
    if (angle>337.5) return 'Northerly';
    if (angle>292.5) return 'North Westerly';
    if(angle>247.5) return 'Westerly';
    if(angle>202.5) return 'South Westerly';
    if(angle>157.5) return 'Southerly';
    if(angle>122.5) return 'South Easterly';
    if(angle>67.5) return 'Easterly';
    if(angle>22.5)return 'North Easterly';
    return 'Northerly';
  }

  componentWillReceiveProps(newProps){
    if(this.socket){
      this.socket.disconnect();
    }
    this.setState({ loading: true, city: newProps.cityId }, this.fetchData(newProps.cityId));
  }

  fetchData(city){
    services.fetch(city).then(data => {
      const endpoint = `http://${process.env.REACT_APP_DOMAIN}:3000/cities/${city.toLowerCase()}`;
      this.socket = socketIOClient(endpoint);
      this.socket.on("forecast", data => {
        this.setState({
          data: data,
          loading: false,
          found: true
        });
      });
    }, reason => {
      this.setState({
        loading: false,
        found: false
      })
    });
  }

  render() {
    return (this.state.loading ? this.renderLoadingDiv() : this.renderInfo());
  }
}

export default CityWeather
