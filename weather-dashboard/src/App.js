import React, { Component } from "react";
import CityWeather from './components/CityWeather'
import { BrowserRouter as Router, Route } from "react-router-dom";
import NavbarBetiware from './components/NavbarBetiware';
import SidebarBetiware from './components/SidebarBetiware';
import Search from './components/Search';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faThermometerThreeQuarters,faWind } from '@fortawesome/free-solid-svg-icons'

library.add(faThermometerThreeQuarters,faWind)

class App extends Component {
  render() {
    return (
      <Router>
      <div className="wrapper">
        <NavbarBetiware></NavbarBetiware>
        <div className="container">
            <div className="row h-100">
              <SidebarBetiware></SidebarBetiware>
              <div className="col-lg-9">
                <div className="row my-3">
                  <div className="col-lg-12">
                    <Route exact path="/" component= {main}/>
                  </div>
                  <div className="col-lg-12">
                    <Route path="/:cityId" component={City} />
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      </Router>
    );
  }
}

function main({match}){
  return <div className="row">
          <div className="col-lg-12">
            <h1 className="mb-4">Welcome to betiware app</h1>
            <h5 className="mb-2">Type and find the weather in your city</h5>
            <Search></Search>
          </div>
        </div>
}

function City({ match }) {
  return <CityWeather cityId={match.params.cityId} />;
}


export default App;
