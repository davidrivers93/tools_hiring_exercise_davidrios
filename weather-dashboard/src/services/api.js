import fetch from 'isomorphic-fetch';

const services = {
  fetch: async (city) => {
    var apiUrl = `http://${process.env.REACT_APP_DOMAIN}:3000/cities/${city.toLowerCase()}`
    const response = await fetch(apiUrl)
    const responseJson = await response.json()
    return responseJson
  }
}

export default services
